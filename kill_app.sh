#kill application
kill $(ps aux |grep oam_agent | grep -v grep | awk '{print $2}')
kill $(ps aux |grep femto.exe | grep -v grep | awk '{print $2}')

#or
kill $(ps aux |grep [o]am_agent | awk '{print $2}')
kill $(ps aux |grep [f]emto.exe | awk '{print $2}')

#or
ps aux |grep [o]am_agent | awk '{print $2}' | xargs kill

#else?